adx-site-check-ce

This is a Chrome extension created to automate some of the checks for proper setup required for displaying of ads.

It goes trough the GPT tags, divs and proprietary script on the site in question.

It has an additional feature to highlight all ad units on a page coloring them and providing an on-click listener to toggle different sizes of the ad unit.

Information is stored using storage API and all errors regarding individual ad units are display in the extension popup window.

One of my first project so totaly laughable by a proper dev. :)